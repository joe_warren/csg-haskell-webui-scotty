{-# LANGUAGE OverloadedStrings, QuasiQuotes, ScopedTypeVariables #-}
module Example (runApp, app) where

import Control.Monad.IO.Class (liftIO)
import           Data.Aeson (Value(..), object, Array, (.=))
import           Network.Wai (Application)
import Network.Wai.Middleware.Static
import Network.Wai.Application.Static
import qualified Web.Scotty as S
import qualified Csg
import qualified Csg.STL as STL
import qualified Data.Vec3 as V3
import qualified Language.Haskell.Interpreter as HS
import Data.List
import System.IO.Temp
import System.IO
import System.Directory
import qualified Data.ByteString.Lazy as BSL
import qualified Data.Text.Encoding as Encoding

runScriptHS :: FilePath -> HS.Interpreter (Csg.BspTree)
runScriptHS tmpDir = do
    HS.set [HS.searchPath HS.:= [tmpDir, "../csg-haskell", "."]]
    sp <- HS.get HS.searchPath
    HS.liftIO $ do
        files <- mapM getDirectoryContents sp
        putStrLn $ show files
    HS.loadModules [tmpDir ++ "/Object.hs"]
    HS.setTopLevelModules ["Object"]
    HS.setImportsQ [("Csg", Nothing)]
    object <- HS.interpret "object" (HS.as::Csg.BspTree)
    return (object)

errorString :: HS.InterpreterError -> String
errorString (HS.WontCompile es) = intercalate "\n" (header : map unbox es)
  where
    header = "ERROR: Won't compile:"
    unbox (HS.GhcError e) = e
errorString e = show e

runScript :: String -> FilePath -> IO (Either String Csg.BspTree)
runScript code tmpDir = do
    writeFile (tmpDir ++ "/Object.hs") code
    r <- HS.runInterpreter $ runScriptHS tmpDir
    case r of
        Left err -> return (Left $ errorString err)
        Right tree -> return $ Right tree


mapTuple3 :: (a -> b) -> (a, a, a) -> (b, b, b)
mapTuple3 f (a1, a2, a3) = (f a1, f a2, f a3)

tupleList :: (a, a, a) -> [a]
tupleList (x, y, z) = [x, y, z]

toJson :: Csg.BspTree -> [Double]
toJson tree = concat $ concat triList
    where
        tris = Csg.toTris tree
        triDoubles = map (mapTuple3 V3.toXYZ) tris
        triList = map (tupleList.(mapTuple3 (tupleList))) triDoubles

csgRoute :: (Csg.BspTree -> S.ActionM ()) -> S.ActionM ()
csgRoute renderer = do
    code::String <- S.param "code"
    liftIO $ putStrLn code
    tree <- liftIO (withSystemTempDirectory "dir.tmp" (runScript code))
    case tree of
      Left err -> do
        liftIO $ do
          putStrLn "haste error:"
          putStrLn err
        S.json $ object [ "error" .= err ]
      Right tree -> do 
        renderer tree

app' :: S.ScottyM ()
app' = do
  S.middleware $ staticPolicy (noDots >-> hasPrefix "static")
  S.get "/" $ S.file "./static/index.html"

  S.post "/build" $ do
      csgRoute $ \tree -> S.json $ toJson tree

  S.post "/export.stl" $ do
      csgRoute $ \tree -> do 
        S.setHeader "Content-Type" "application/sla"
        S.raw (BSL.fromStrict $ Encoding.encodeUtf8 $ STL.toSTL tree)

app :: IO Application
app = S.scottyApp app'

runApp :: IO ()
runApp = S.scotty 8080 app'
