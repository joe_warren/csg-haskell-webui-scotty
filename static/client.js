function init3js() {
    var canvas = document.getElementById("threeCanvas");
    camera = new THREE.PerspectiveCamera( 60, canvas.offsetWidth / window.innerHeight, 10, 10000 );
    camera.position.z = 500;
    controls = new THREE.TrackballControls( camera );
    controls.rotateSpeed = 1.0;
    controls.zoomSpeed = 1.2;
    controls.panSpeed = 0.8;
    controls.noZoom = false;
    controls.noPan = false;
    controls.staticMoving = true;
    controls.dynamicDampingFactor = 0.3;
    controls.keys = [ 65, 83, 68 ];
    controls.addEventListener( 'change', render );
    // world
    scene = new THREE.Scene();
    //scene.background = new THREE.Color( 0x272822 );
    // lights
    var light = new THREE.DirectionalLight( 0xdddddd );
    light.position.set( -250, 500, 0 );
    camera.add( light );
    var light = new THREE.DirectionalLight( 0x220044 );
    light.position.set( 250, -500, -500 );
    camera.add( light );
    var light = new THREE.AmbientLight( 0x222222 );
    scene.add(light);
    scene.add(camera)
    // renderer
    renderer = new THREE.WebGLRenderer( { antialias: true, alpha: true } );
    renderer.autoClear = false;
    renderer.setClearColor(0x000000, 0.0);
    renderer.setPixelRatio( window.devicePixelRatio );
    renderer.setSize( canvas.offsetWidth, window.innerHeight );
    canvas.appendChild( renderer.domElement );
		
    window.addEventListener( 'resize', onWindowResize, false );
	
	render();
}
function onWindowResize() {
    var canvas = document.getElementById("threeCanvas");
    camera.aspect = canvas.offsetWidth / window.innerHeight;
    camera.updateProjectionMatrix();
    renderer.setSize( canvas.offsetWidth, window.innerHeight );
    controls.handleResize();
    render();
}
function animate() {
    requestAnimationFrame( animate );
    controls.update();
}
function render() {
    renderer.clear()
    renderer.render( scene, camera );
}

function flatten(arr) {
  return arr.reduce(function (flat, toFlatten) {
    return flat.concat(Array.isArray(toFlatten) ? flatten(toFlatten) : toFlatten);
  }, []);
}
function showSpinner() {
    var c = document.getElementById("loading");
    c.style.display = "block";
}

function hideSpinner() {
    var c = document.getElementById("loading");
    c.style.display = "None";
}

showWireframe = false;
function toggle_wireframe(){
    showWireframe = !showWireframe;
    if(lineSegments){
        lineSegments.visible = showWireframe;
        render();
    }
}

function close_error(){
    document.getElementById("error").style.display = "none";
}

function raise_error_text(text){
    document.getElementById("errorText").innerText = text;
    document.getElementById("error").style.display = "block";
}

mesh = undefined;
lineSegments = undefined;
function build_mesh(){
    var current_code = editor.getValue();
    showSpinner();
    $.post("/build",
    {
        code: current_code
    }).done(
    function(data){
        var geometry = new THREE.BufferGeometry();
        var vertices = data
        geometry.addAttribute( 'position', new THREE.BufferAttribute( 
            new Float32Array(vertices), 3 ) );
        geometry.scale(100, 100, 100);
        var material = new THREE.MeshStandardMaterial( { 
            color: 0xffffff, 
            flatShading: true,
            polygonOffset: true,
            polygonOffsetFactor: 1,
            polygonOffsetUnits: 1
        } );

		var lineGeometry = new THREE.WireframeGeometry(geometry );
        var lineMaterial = new THREE.LineBasicMaterial({
            color:0x000000, 
            linewidth: 2,
        });
        if(mesh){
            scene.remove(mesh);
            scene.remove(lineSegments);
        }
        lineSegments = new THREE.LineSegments(lineGeometry, lineMaterial);
		lineSegments.computeLineDistances();

        scene.add(lineSegments);
        lineSegments.visible = showWireframe;

        mesh = new THREE.Mesh( geometry, material );
        scene.add(mesh);

        render();
        hideSpinner();
    }).fail(function(xhr, status, data){
        raise_error_text(xhr.responseText);
        hideSpinner();
    });
}

function export_mesh(){
    var form = $('<form></form>')
            .attr('action', "/export.stl")
            .attr('method', 'post');
    form.append($("<input></input>")
            .attr('type', 'hidden')
            .attr('name', "code")
            .attr('value', editor.getValue()));
    //send request
    form.appendTo('body').submit().remove();
}

window.onload = function(){
    editor = ace.edit("editor", {
        mode:"ace/mode/haskell",
        wrap: 80
    });
    editor.setTheme("ace/theme/monokai");
    init3js();
    animate();
}
